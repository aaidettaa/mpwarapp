# Instalacion

En el ```composer.json``` :

```json
{
  "repositories": [
        {
          "type": "vcs",
          "url": "https://bitbucket.org/aaidettaa/mpwarframework"
        }
    ],
 "require": {
        "aaidettaa/mpwarframework": "dev-master"
    }
}
```

# Virtualhost

```shell
<VirtualHost *:80>
        ServerName mpwarappfwk.dev
        DocumentRoot "/var/www/mpwarapp/public"
        <Directory "/var/www/mpwarapp/public">
                Options Indexes FollowSymLinks MultiViews
                AllowOverride None
                Require all granted
        </Directory>
        ErrorLog "/var/log/httpd/mpwarappfwk.dev_error.log"
        ServerSignature Off
        CustomLog "/var/log/httpd/mpwarappfwk.dev_access.log" combined
        FallbackResource /app.php
        RewriteEngine On
</VirtualHost>
```

# DataBase

```sql
CREATE DATABASE IF NOT EXISTS fwk 
USE fwk;
CREATE TABLE IF NOT EXISTS Product (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=INNODB;
```