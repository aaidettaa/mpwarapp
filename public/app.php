<?php

require_once __DIR__ . '/../vendor/autoload.php';

use mpwarframework\framework\Component\Bootstrap\Bootstrap;
use mpwarframework\framework\Component\Http\Request\Request;

$app = new Bootstrap(__DIR__ . '/../');
$request = Request::createFromGlobals($_REQUEST, $_FILES, $_SERVER, $_COOKIE, $_GET, $_POST);
$app->run($request);


