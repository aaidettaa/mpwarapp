<?php

namespace myAppMpwar\Controllers;


use mpwarframework\framework\Component\Container\Container;
use mpwarframework\framework\Component\Controller\Controller;
use mpwarframework\framework\Component\Http\Request\Request;

class DefaultController extends Controller
{

    public function index(Request $request)
    {
        $obj = $this->container->get('test2');
        $contenido = '<html><body><h1>INDEX '.$obj->main().'</h1></body></html>';
        $this->response->setContent($contenido);
        return $this->response;
    }

    public function test(Request $request)
    {
        $this->response->setContent($this->templating->show('test'));
        return $this->response;
    }

    public function test2(Request $request)
    {
        $repository = $this->container->get(Container::REPOSITORY_CONTAINER);
        $repository->save('Product', ['name' => 'Camisa']);
        $result_set = $repository->find('Product', ['name' => 'Camisa']);
        $this->templating->assign('product', $result_set[1]);
        $this->response->setContent($this->templating->show('test2'));
        return $this->response;
    }

}